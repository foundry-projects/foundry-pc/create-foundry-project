import path from 'path';
import fs from 'fs-extra';
import chalk from 'chalk';
import ora from 'ora';

export default async (options: ProjectOptions) => {
	const spinner = ora(`Creating ${chalk.green('package.json')}`).start();

	const packageJson = {
		private: true,
		name: options.name,
		version: '0.1.0',
		description: '',
		scripts: {
			package: 'gulp package'
		},
		author: '',
		license: '',
		devDependencies: {}
	};

	if (options.useTypeScript || options.cssPrep) {
		Object.assign(packageJson.scripts,
			{
				build: 'gulp build && gulp link',
				'build:watch': 'gulp watch',
				clean: 'gulp clean && gulp link --clean'
			});
	}

	if (options.useTypeScript) {
		Object.assign(packageJson.scripts,
			{
				update: 'npm install --save-dev gitlab:foundry-projects/foundry-pc/foundry-pc-types'
			});
	}

	if (options.useLint) {
		Object.assign(packageJson.scripts, { lint: 'gulp lint' });
	}

	try {
		await fs.writeJSON(path.join(options.dir, 'package.json'), packageJson, { spaces: 2 });
	} catch (err) {
		spinner.stop();
		throw err;
	}

	spinner.succeed(chalk.green('Created package.json'));
};
