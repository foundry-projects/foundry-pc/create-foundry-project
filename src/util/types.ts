/**
 * Set of options to configure the project
 */
interface ProjectOptions {
	/** The root directory to create the project in */
	dir: string;

	/** The name of the project (basename of the root directory) */
	name: string;

	/** Whether the project is a system instead of a module */
	isSystem?: boolean;

	/** Whether to configure the project to use TypeScript */
	useTypeScript?: boolean;

	/** Whether to use a CSS preprocessor (Less or Sass) */
	cssPrep?: 'less' | 'sass';

	/** Whether to use linting */
	useLint?: boolean;

	/** Whether to install project dependencies */
	deps?: boolean;

	/** Whether to initialize Git repository */
	git?: boolean;
}
